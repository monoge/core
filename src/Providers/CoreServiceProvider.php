<?php

namespace Core\Providers;

use Core\Console\Up;
use Core\Controllers\Kernel;
use Core\Foundation\Helper;
use Illuminate\Contracts\Http\Kernel as HttpKernel;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Up::class
            ]);
        }

        App::setLocale('tr');


        $this->loadMigrationsFrom(__DIR__.'/../Database/migrations');
        $this->loadTranslationsFrom(__DIR__.'/../Translations', 'core');
        $this->loadViewsFrom(__DIR__.'/../Resources/views', 'core');
        
        $this->publishes([
            __DIR__.'/../Resources/assets' => public_path('assets'),
        ], 'core');
    }

    public function register()
    {

        $this->mergeConfigFrom(
            __DIR__.'/../Config/app.php', 'core'
        );

        $this->app->singleton(HttpKernel::class, Kernel::class);

        $this->app->register(RouteServiceProvider::class);

        $loader = AliasLoader::getInstance();
        $loader->alias('Helper',Helper::class);

    }
}
