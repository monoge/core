<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('guest');
//    }

    public function showResetForm($monster)
    {
        $count = DB::table('password_resets')->select('email')->count();

        return $monster->fire('web.account.auth.reset-password',[
            'token' => $monster->request->get('token'),
            'count' => $count
        ]);
    }

    public function reset($request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email|exists:users',
            'password' => 'required|confirmed|min:6',
        ],[
            'token.required' => ':attribute gönderilmesi zorunlu alandır.',
            'email.required' => ':attribute bölümünü doldurmalısınız.',
            'email.email' => ':attribute geçerli değil.',
            'email.exists' => ':attribute sistemimizde kayıtlı değil.',
            'password.required' => ':attribute ile herhangi bir kullanıcımız bulunmuyor.',
            'password.confirmed' => ':attribute eşleşmiyor, kontrol ediniz.',
            'password.min' => ':attribute en az :min karakter olmalıdır.',
        ],[
            'token' => 'Doğrulama Kodu',
            'email' => 'E-posta Adresiniz',
            'password' => 'Yeni Şifreniz',
        ]);

        try {
            $response = $this->broker()->reset(
                $this->credentials($request), function ($user, $password) {
                    $this->resetPassword($user, $password);
                }
            );
        } catch (\Exception $e){
            return response()->json(['errors' => ['Bir hata oluştu, lütfen iletişim bölümünden bize ulaşınız.']],422);
        }

        if($response == Password::PASSWORD_RESET){
            $redirect = web();
            return response()->json($redirect,200);
        }else{
            return response()->json(['errors' => [trans($response)]],422);
        }

    }
}
