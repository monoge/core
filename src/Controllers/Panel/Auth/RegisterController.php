<?php

namespace App\Http\Controllers\Web\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Models\UserGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
/*
    public function __construct()
    {
        $this->middleware('guest');
    }
*/
    public function showRegistrationForm($monster)
    {
        return $monster->fire('web.account.auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $titles = [
//            'name'      => 'Adınız',
//            'surname'   => 'Soyadınız',
            'fullname'  => 'Adınız Soyadınız',
            'email'     => 'E-posta Adresiniz',
            'password'  => 'Şifreniz',
        ];
        return Validator::make($data, [
//            'name'      => 'required|string|min:2|max:50',
//            'surname'   => 'required|string|min:2|max:50',
            'fullname'  => 'required|string|min:2|max:50',
            'email'     => 'required|string|email|unique:users|min:6|max:255',
            'password' => 'required|string|min:6|max:50|confirmed',
        ],[],$titles);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $name = explode(' ',$data['fullname']);
        $surname = array_pop($name);
        $name = implode(' ',$name);

        $customer_group_default = UserGroup::where('default',1)->first();

        return User::create([
            'user_group_id' => ($customer_group_default ? $customer_group_default->id : 1),
            'language_id' => 1,
            'name' => htmlspecialchars(strip_tags($name)),
            'surname' => htmlspecialchars(strip_tags($surname)),
            'email' => htmlspecialchars(strip_tags($data['email'])),
            'password' => Hash::make($data['password']),
        ]);
    }

    protected function registered(Request $request, $user)
    {
        $redirect = web();

        $requestNext = $request->get('next');
        if( $requestNext != null ){
            if( decryptHelper($requestNext) == 'order-information' ){
                $redirect = web('siparis/bilgiler.xhtml');
            }
        }

        return response()->json($redirect,200);
    }
}
