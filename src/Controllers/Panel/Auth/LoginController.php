<?php

namespace Core\Controllers\Panel\Auth;

use Core\Controllers\Panel\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function showLoginForm()
    {
        return view('core::panel.auth.login');
    }

    protected function validateLogin(Request $request)
    {
        $request = $request->all();

        $validate = Validator::make($request, [
            $this->username() => 'required|string|min:6|max:255',
            'password' => 'required|string|min:6|max:50',
        ]);

        if($validate->fails()){
            return response()->json(trans('auth.failed'),422);
        }
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        return response()->json(trans('auth.failed'),422);
    }

    protected function authenticated(Request $request, $user)
    {
        if($user->status!=1){
            return $this->logoutJson($request);
        }

        $slug = $user->admin == 1 ? 'hesabim.xhtml' : null;
        $redirect = web( $slug );

        $requestNext = $request->get('next');
        if( $requestNext != null ){
            if( decryptHelper($requestNext) == 'order-information' ){
                $redirect = web('siparis/bilgiler.xhtml');
            }
        }

        return response()->json($redirect,200);
    }

    public function logoutJson(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return response()->json(trans('auth.failed'),422);
    }

    public function logoutCustom($monster)
    {
        return $this->logout($monster->request);
    }

    public function username()
    {
        return 'email';
    }

    public function check_email($monster)
    {
        $email = $monster->request->get('email');
        if( !$email ){
            return response()->json(null,404);
        }

        $request = [
            'email' => $email,
        ];

        $validate = Validator::make($request, [
            'email' => 'required|email|min:6|max:255',
        ]);

        if( $validate->fails() ){
            return response()->json(null,422);
        }

        $user = User::whereNull('deleted_at')
            ->where('status',1)
            ->where('email',$email)
            ->first();

        if( $user ){
            return response()->json(1,200); //true
        }else{
            return response()->json(0,200); //false
        }

    }

}
