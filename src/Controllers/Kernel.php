<?php

namespace Core\Controllers;

use Core\Middleware\Panel;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{

    protected $routeMiddleware = [
        'panel' => Panel::class
    ];

}
