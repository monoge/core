<?php
// Namespace: Core\Controllers\Panel

Route::get('login','Auth\LoginController@showLoginForm')->name('login');

Route::get('/','HomeController@index')->name('home');
