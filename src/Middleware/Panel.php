<?php

namespace Core\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Panel
{

    public function handle($request, Closure $next)
    {
        if ( !Auth::guest() || in_array( $request->route()->getName(), ['login','register'] ) )
        {
            return $next($request);
        }

        return redirect()->route('login');
    }

}
