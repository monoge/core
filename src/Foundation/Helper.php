<?php

namespace Core\Foundation;

class Helper
{

    public static function asset( $url = '/' )
    {
        return 'assets/'.$url;
    }

}
