<?php

namespace Core\Console;

//use App\Models\App;
//use App\Models\Database;
//use App\Models\Server;
use Illuminate\Console\Command;

class Up extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:up {process} {operation1=default} {operation2=default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Veritabanı kurulum işlemleri';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dd(uniqid());
        $command='migrate';
        $arr=[
            '--path' => 'database/migrations/'
        ];

        $process = $this->argument('process');
        $operation1 = $this->argument('operation1');
        $operation2 = $this->argument('operation2');

        if($process=='main'){
            $arr['--path'] .= 'mysql';
            $arr['--database'] = 'mysql';
            $this->afterProcess($command,$arr,$operation1,$operation2);
        }elseif($process=='new'){
            $this->newProcess($command,$arr,$operation1,$operation2);
        }else{

            $app=App::where('code',$process)->first();
            if(!$app){
                $this->info('Veritabanında '.$process.' kodlu app bulunamadı!'); exit;
            }
            $server=$app->server;
            $db=$app->code.env('APP_SUFFIX');
            connect('remote',$server->ip,$server->port,$db,$db,ucfirst($server->slug).env('PASSWORD_SUFFIX'));

            $arr['--path'] .= 'remote';
            $arr['--database'] = 'remote';
            $this->afterProcess($command,$arr,$operation1,$operation2);
            connect('remote');

        }

/*
        if($process=='main'){
            $this->mainProcess($command,$arr,$operation1,$operation2);
        }elseif($process=='customer'){
            $this->customerProcess($command,$arr,$operation1,$operation2);
        }
*/
    }

    public function afterProcess($command,$arr,$operation1,$operation2)
    {
        // fresh, refresh
        if($operation1=='fresh'){
            $command.=':fresh';
        }elseif($operation1=='refresh'){
            $command.=':refresh';
        }elseif($operation2=='default'){
            $operation2=$operation1; // "db:install main seed" kullanımı için kelime oyunu
        }

        $return = $this->ask('Yapılacak veritabanı işlemini onaylıyor musunuz? [e/h]');

        if($return=='e'){
            \Artisan::call($command,$arr);

            if($operation2=='seed'){
                try{
                    \Artisan::call('db:seed',['--class'=> ($arr['--database']=='remote'?'RemoteSeeder':'DatabaseSeeder') ]);
                }catch (\Exception $e){
                    $this->error('Başarısız, seed yapılamadı muhtemelen aynı kayıtlar var !'); exit;
                }
            }
            $this->comment('Başarılı, işlem tamamlandı !');
        }else{
            $this->error('Doğru yanıt verilemediğinden, işlem yapılamadı !');
        }
    }

    public function newProcess($command,$arr,$operation1,$operation2)
    {
        $command.=':fresh';
        $arr['--path'] .= 'remote';
        $arr['--database'] = 'remote';

        if($operation1!='default'){

            $server = Server::where('type','database')
                ->where('status',1)
                ->where('slug',$operation1)
                ->first();

            if(!$server){ dd($operation1.' slug\'ında sunucu bulunamadı..!'); }

        }else{

            $server = Server::where('type','database')
                ->where('status',1)
                ->withCount('apps')
                ->get()
                ->filter(function ($item) {
                    return $item['apps_count'] != $item['limit'];
                })
                ->sortBy('apps_count')->first();

            if(!$server){ dd('Yeterli sunucu yok..!'); }

        }

        connect('root',$server->ip,$server->port,'',$server->login,$server->password);

        $code = $this->appCodeControl($server->id);
        $db = $code.env('CUSTOMER_DB_NAME_SUFFIX');
        $pass = ucfirst($server->slug).env('CUSTOMER_DB_PASSWORD_SUFFIX');

        \DB::connection('root')->statement("CREATE DATABASE `".$db."`;");
        \DB::connection('root')->statement("CREATE USER '".$db."'@'%' IDENTIFIED BY '".$pass."';");
        \DB::connection('root')->statement("GRANT ALL PRIVILEGES ON ".$db.".* TO '".$db."'@'%';");
        \DB::connection('root')->statement("FLUSH PRIVILEGES;");
        connect('root');

        //
        connect('remote',$server->ip,$server->port,$db,$db,$pass);

        \Artisan::call($command,$arr);
        connect('remote');

        $this->comment('Başarılı, '.$code.' kodu ile apps tablosuna oluşturuldu !');

    }

    public function appCodeControl($server_id,$code=null)
    {
        if( !$code ) { $code=uniqid(); }

        try {
            \DB::table('apps')
                ->insert([
                    'server_id' => $server_id,
                    'code' => $code,
                    'status' => 0
                ]);
        } catch (\Exception $e) {
             return $this->appCodeControl($server_id);
        }

        return $code;
    }

}
